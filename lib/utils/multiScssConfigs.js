'use strict'

const webpackConfig = require('./webpackConfig');
const path = require('path');
const fs = require('fs');
const helpers = require('../helpers');

const cwd = process.cwd();

var scssFiles = helpers.createScssPath();

function getNames() {
    return Object.keys(scssFiles).map(
        function(fileKey) {
            let pathLevels = fileKey.split(path.sep);
            return 'scss-' + pathLevels.shift() + '-' + pathLevels.pop();
        }
    );
}

function getEntry(name) {
    let entry = {};
    let fileKey = Object.keys(scssFiles).find(
        function(fileKey) {
            let pattern = new RegExp(name.replace(/^scss-.+?-/,'') + '$');
            return pattern.test(fileKey);
        }
    );

    entry[fileKey] = scssFiles[fileKey];
    return entry;
}

function getConfigs() {
    const packageFile = require(path.join(cwd, './package.json'));

    let configs = [];
    getNames().forEach(
        function(name) {
            var scssConfig = Object.assign({}, webpackConfig(packageFile, cwd, 'scss'));
            if (typeof scssConfig === Error) {
                return;
            }
            scssConfig.name = name;
            scssConfig.entry = getEntry(name);
            configs.push(scssConfig);
        }
    );

    return configs;
}

module.exports = {
    getConfigs
};